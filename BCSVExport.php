<?php

Yii::import("zii.widgets.grid.CGridView");

class BCSVExport extends CGridView
{


    /**
     * Export starts immediately
     * @var bool
     */
    public $export = false;
    public $exportKey = 'export';
    public $separator = ';';

    public $template = "{buttons}\n{summary}\n{items}\n{pager}";

    public $filename = 'export-csv';


    public function init()
    {
        if (!$this->export) {
            $params = Yii::app()->getController()->actionParams;
            $this->export = (bool)isset($params[$this->exportKey]);
        }

        parent::init();
    }


    public function renderItems()
    {


        if (!$this->export) {
            parent::renderItems();
        } else {
            $this->cleanOutput();

            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Type: application/csv');
            header('Cache-Control: max-age=0');
            header('Content-Disposition: attachment; filename="' . $this->filename . '.csv"');


            $this->renderCSVHeader();
            $this->renderCSVBody();
            $this->renderCSVFooter();

            ob_start();
            Yii::app()->end();
            ob_end_clean();
        }

    }


    protected function renderCSVHeader()
    {
        foreach ($this->columns as $column) {
            echo $this->_outData($column->getHeaderCellContent()), $this->separator;
        }
        echo PHP_EOL;
    }

    private function _outData($string)
    {
        return '"' . Chtml::encode(strip_tags($string)) . '"';
    }

    protected function renderCSVBody()
    {

        $data = $this->dataProvider->getData();
        $n = count($data);
        if ($n > 0) {
            for ($row = 0; $row < $n; ++$row) {
                foreach ($this->columns as $column) {

                    ob_start();
                    $column->getDataCellContent($row);
                    $txt = ob_get_clean();
                    echo $this->_outData($txt), $this->separator;
                }
                echo PHP_EOL;
            }
        }
    }

    protected function renderCSVFooter()
    {
        foreach ($this->columns as $column) {
            ob_start();
            $column->getFooterCellContent();
            $txt = ob_get_clean();
            echo $this->_outData($txt), $this->separator;
        }
        echo PHP_EOL;
    }


    /**
     * Performs cleaning on mutliple levels.
     *
     */
    private static function cleanOutput()
    {
        for ($level = ob_get_level(); $level > 0; --$level) {
            @ob_end_clean();
        }
    }

    protected  function renderButtons()
    {
        echo CHtml::link('Export to csv', array('', 'export' => 'csv') + $this->getController()->actionParams);
    }
}